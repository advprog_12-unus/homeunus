package com.unus.homeunus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/")
    private String homepage() {
        return "home";
    }

    @GetMapping("/notes")
    @ResponseBody
    private String notespage() {
        String notelist = restTemplate.getForObject("http://UNUSNOTES-SERVICE/allnotes", String.class);
        return notelist;
    }

//    @GetMapping("/hello")
//    private String hello() {
//        return "User/Hello";
//    }
//
//    @GetMapping("/login")
//    private String login() {
//        return "User/login";
//    }
}
